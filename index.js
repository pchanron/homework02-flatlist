/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import MyList from './src/UserInfo/MyList';

AppRegistry.registerComponent('homework02_flatList', () => MyList);
