import React, { Component } from 'react';
import { Container, Header, Body, Title, Content } from 'native-base';
import MyCard  from './MyCard';
import { FlatList } from 'react-native';
const picProfile = require("../Images/user_picProfile.png");

export default class MyList extends Component {

    state = {
        Users: [
            {
                key: 1,
                name: 'Chanron',
                age: 22,
                image: picProfile
            },
            {
                key: 2,
                name: 'Je Ploy',
                age: 25,
                image: picProfile
            },
            {
                key: 3,
                name: 'Lida',
                age: 22,
                image: picProfile
            }
        ]
    }
    render() {

        return (
            <Container>
                <Header>
                    <Body>
                        <Title>User Info</Title>
                    </Body>
                </Header>
                <Content>
                    <FlatList
                        data={this.state.Users}
                        renderItem={({ item }) => <MyCard name={item.name} age={item.age} image={item.image} />} />
                </Content>
            </Container>
        );
    }
}