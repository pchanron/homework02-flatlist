
import React from 'react';
import { Card, CardItem, Thumbnail, Text, Left, Body } from 'native-base';

const MyCard = (props) => (

  <Card>
    <CardItem>
      <Left>
        <Thumbnail large source={props.image} />
        <Body>
          <Text>{props.name}</Text>
          <Text note>{props.age}</Text>
        </Body>
      </Left>
    </CardItem>
  </Card>

);

export default MyCard 